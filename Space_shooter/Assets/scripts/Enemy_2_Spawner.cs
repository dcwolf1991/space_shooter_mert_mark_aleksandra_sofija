using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_2_Spawner : MonoBehaviour

{

    public GameObject Enemy_2;



    float maxSpawnRateInSeconds = 10f;



    

    void Start()

    {

        Invoke("SpawnEnemy", maxSpawnRateInSeconds);



        InvokeRepeating("IncreaseSpawnRate", 0f, 50f);

    }



    

    void Update()

    {



    }



    void SpawnEnemy()

    {

        Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2(0, 0));



        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));



        GameObject anEnemy = (GameObject)Instantiate(Enemy_2);

        anEnemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);



        ScheduleNextEnemySpawn();

    }



    void ScheduleNextEnemySpawn()

    {

        float spawnInNSeconds;



        if (maxSpawnRateInSeconds > 10f)

        {

            spawnInNSeconds = Random.Range(10f, maxSpawnRateInSeconds);

        }

        else

            spawnInNSeconds = 10f;



        Invoke("SpawnEnemy", spawnInNSeconds);

    }



    void IncreaseSpawnRate()

    {

        if (maxSpawnRateInSeconds > 5f)

            maxSpawnRateInSeconds--;



        if (maxSpawnRateInSeconds == 5f)

            CancelInvoke("IncreaseSpawnRate");

    }



}